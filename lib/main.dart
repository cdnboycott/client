import 'package:flutter/material.dart';
import 'package:grocerycompare/component/pirce_list.dart';
import 'package:grocerycompare/view/add.dart';
import 'package:grocerycompare/view/bounty.dart';
import 'package:grocerycompare/view/groceries.dart';
import 'color_schemes.g.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';


Future<void> main() async {
   await dotenv.load(fileName: ".env");
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      themeMode: ThemeMode.dark,
      theme: ThemeData(useMaterial3: true, colorScheme: lightColorScheme),
      darkTheme: ThemeData(useMaterial3: true, colorScheme: darkColorScheme),
      home: const Grocerycompare(),
    );
  }
}

class Grocerycompare extends StatelessWidget {
  const Grocerycompare({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      themeMode: ThemeMode.dark,
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          bottomNavigationBar: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.map)),
              Tab(icon: Icon(Icons.local_grocery_store)),
              Tab(icon: Icon(Icons.search)),
              Tab(icon: Icon(Icons.addchart_outlined)),
            ],
          ),
          body: SafeArea(child:  TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: [
              TargetPage(),
              Pricelist(),
              BountyPage(title: 'Bounty'),
              AddPage(title: 'Add new item')
            ],
          )),
        ),
      ),
    );
  }
}
