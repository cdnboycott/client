import 'dart:convert';

import 'package:grocerycompare/data/searchbox.dart';
import 'package:http/http.dart' as http;
import 'package:mapbox_gl/mapbox_gl.dart';

class MapService{ 
  Future<SearchboxResponse> getNearby(LatLngBounds bounds, String mapApiKey, int limit) async {

      var neLat = bounds.northeast.latitude;
      var neLong = bounds.northeast.longitude;
      var swLate = bounds.southwest.latitude;
      var swLong = bounds.southwest.longitude;

      final Uri uri = Uri.parse('https://api.mapbox.com/search/searchbox/v1/category/grocery?access_token=$mapApiKey&language=en&limit=$limit&bbox=$swLong%2C$swLate%2C$neLong%2C$neLat');
      var response = await http.get(uri);
      return SearchboxResponse.fromJson(jsonDecode(response.body));
  }
}
