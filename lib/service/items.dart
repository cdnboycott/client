import 'dart:convert';

import 'package:grocerycompare/data/item.dart';
import '../val.g.dart' as globals;
import 'package:http/http.dart' as http;

class Items {
  Future<List<Item>> getItemsAtStore(String storeId) async {
    final Uri uri =
        Uri.parse('${globals.baseURL}items/fromStore?storeId=$storeId');
    var response = await http.get(uri);
    return sampleFromJson(response.body);
  }

  Future<List<Item>> getRelatedItems(String barcode) async {
    final Uri uri =
        Uri.parse('${globals.baseURL}items/related?barcode=$barcode');
    var response = await http.get(uri);
    return sampleFromJson(response.body);
  }

  Future<bool> postItemsAtStore(Map<String, dynamic> data) async {
    final Uri uri = Uri.parse('${globals.baseURL}item');

    var r = await http.post(uri, body: json.encode(data), headers: {
      "Content-Type": "application/json",
      'accept': 'application/json'
    });

    if (r.statusCode >= 200 && r.statusCode <= 299) {
      return true;
    }

    return false;
  }
}
