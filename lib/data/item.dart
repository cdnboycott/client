
import 'dart:convert';

List<Item> sampleFromJson(String str) {
    final jsonData = json.decode(str);
    return List<Item>.from(jsonData.map((x) => Item.fromJson(x)));
}

// String sampleToJson(List<Item> data) {
//     final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
//     return json.encode(dyn);
// }

class Item {
  final String name;
  final int cost;
  final int weight;
  final String unit;
  final String barcode;
  final String storeId;
  final String storeName;


  const Item({
    required this.name,
    required this.cost,
    required this.weight,
    required this.unit,
    required this.barcode,
    required this.storeId,
    required this.storeName,
  });

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      name: json['Name'] as String,
      cost: json['Cost'] as int,
      weight: json['Weight'] as int,
      unit: json['Unit'] as String,
      barcode: json['Barcode'] as String,
      storeId: json['StoreId'] as String,
      storeName: json['StoreName'] as String,
    );
  }
}
            