
import 'dart:convert';

List<Tag> sampleFromJson(String str) {
    final jsonData = json.decode(str);
    return List<Tag>.from(jsonData.map((x) => Tag.fromJson(x)));
}

class Tag {
  final String name;


  const Tag({
    required this.name,
  });

  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(
      name: json['Name'] as String,
    );
  }
}
            