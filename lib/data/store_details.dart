
import 'dart:convert';

List<StoreDetails> sampleFromJson(String str) {
    final jsonData = json.decode(str);
    return List<StoreDetails>.from(jsonData.map((x) => StoreDetails.fromJson(x)));
}

class StoreDetails {
  final String storeId;
  final String storeName;
  final String storeNeightbourhood;


  const StoreDetails({
    required this.storeId,
    required this.storeName,
    required this.storeNeightbourhood,
  });

  factory StoreDetails.fromJson(Map<String, dynamic> json) {
    return StoreDetails(
      storeId: json['StoreId'] as String,
      storeName: json['StoreName'] as String,
      storeNeightbourhood: json['StoreNeightbourhood'] as String,
    );
  }
}
            
            