class SearchboxResponse {
  List<Feature>? results;
  String? status;

  SearchboxResponse({this.results, this.status});

  SearchboxResponse.fromJson(Map<String, dynamic> json) {
    if (json['features'] != null) {
      results = <Feature>[];
      json['features'].forEach((v) {
        results!.add(Feature.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (results != null) {
      data['features'] = results!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Feature {
  Geometry? geometry;
  String? type;
  Properties? properties;

  Feature({
    this.geometry,
    this.type,
    this.properties,
  });

  Feature.fromJson(Map<String, dynamic> json) {
    geometry =
        json['geometry'] != null ? Geometry.fromJson(json['geometry']) : null;
    type = json['type'];
    properties = json['properties'] != null
        ? Properties.fromJson(json['properties'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (geometry != null) {
      data['geometry'] = geometry!.toJson();
    }
    data['type'] = type;
    data['properties'] = properties;

    if (properties != null) {
      data['properties'] = properties!.toJson();
    }

    return data;
  }
}

class Geometry {
  Coordinates? coordinates;
  String? type;

  Geometry({this.coordinates, this.type});

  Geometry.fromJson(Map<String, dynamic> json) {
    coordinates = json['coordinates'] != null
        ? Coordinates.fromJson(json['coordinates'])
        : null;
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (coordinates != null) {
      data['coordinates'] = coordinates!.toJson();
    }

    data['type'] = type;

    return data;
  }
}

class Properties {
  String? name;
  String? mapbox_id;
  String? feature_type;
  String? address;
  String? full_address;
  String? place_formatted;
  Map<String, dynamic>? context;
  Coordinates? coordinates;
  String? language;
  String? maki;
  List<dynamic>? poi_category;
  List<dynamic>? poi_category_ids;
  Map<String, dynamic>? external_ids;
  MetaData? metadata;

  Properties(
      {this.name,
      this.mapbox_id,
      this.feature_type,
      this.address,
      this.full_address,
      this.place_formatted,
      this.context,
      this.coordinates,
      this.language,
      this.poi_category,
      this.poi_category_ids,
      this.external_ids,
      this.metadata});

  Properties.fromJson(Map<String, dynamic> json) {
    name = json["name"];
    mapbox_id = json["mapbox_id"];
    feature_type = json["feature_type"];
    address = json["address"];
    full_address = json["full_address"];
    place_formatted = json["place_formatted"];
    context = json["context"];

    coordinates = json['coordinates'] != null
        ? Coordinates.fromJson(json['coordinates'])
        : null;

    language = json["language"];
    poi_category = json["poi_category"];
    poi_category_ids = json["poi_category_ids"];
    external_ids = json["external_ids"];
    metadata =
        json['metadata'] != null ? MetaData.fromJson(json['metadata']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['mapbox_id'] = mapbox_id;
    data['feature_type'] = feature_type;
    data['address'] = address;
    data['full_address'] = full_address;
    data['place_formatted'] = place_formatted;
    data['context'] = context;
    if (coordinates != null) {
      data['coordinates'] = coordinates!.toJson();
    }

    data['language'] = language;
    data['poi_category'] = poi_category;
    data['poi_category_ids'] = poi_category_ids;
    data['external_ids'] = external_ids;
    if (metadata != null) {
      data['metadata'] = metadata!.toJson();
    }
    return data;
  }
}

class SimpleCoords {
  double? latitude;
  double? longitude;

  SimpleCoords({this.latitude, this.longitude});

  SimpleCoords.fromJson(List<dynamic> json) {
    latitude = json[0];
    longitude = json[1];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    return data;
  }
}

class Coordinates {
  double? latitude;
  double? longitude;
  List<RoutablePoints>? routable_points;

  Coordinates({this.latitude, this.longitude, this.routable_points});

  Coordinates.fromJson(dynamic json) {
    if (json is Map) {
      latitude = json['latitude'];
      longitude = json['longitude'];
      if (json.length >= 3 && json['routable_points'] != null) {
        routable_points = List.empty(growable: true);
        for (var p in json['routable_points']) {
          routable_points?.add(RoutablePoints.fromJson(p));
        }
      }
    } else {
      latitude = json[1];
      longitude = json[0];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    //data['routable_points'] = routable_points;

    if (routable_points != null) {
      data['routable_points'] = List.empty(growable: true);
      for (var p in routable_points!) {
        data['routable_points'].add(p.toJson());
      }
    }

    return data;
  }
}

class RoutablePoints {
  String? name;
  double? latitude;
  double? longitude;

  RoutablePoints({this.latitude, this.longitude, this.name});

  RoutablePoints.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longitude = json['longitude'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['name'] = name;
    return data;
  }
}

class MetaData {
  String? phone;
  String? website;
  String? name;
  OpenHours? openHours;

  MetaData({this.phone, this.website, this.name, this.openHours});

  MetaData.fromJson(Map<String, dynamic> json) {
    phone = json['phone'];
    website = json['website'];
    name = json['name'];
    openHours = json['openHours'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['phone'] = phone;
    data['website'] = website;
    data['name'] = name;
    data['openHours'] = openHours;
    return data;
  }
}

class OpenHours {
  String? name;

  OpenHours({this.name});

  OpenHours.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['0'] = name;
    return data;
  }
}
