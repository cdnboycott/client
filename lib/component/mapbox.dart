// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:grocerycompare/component/map_info.dart';
import 'package:grocerycompare/data/searchbox.dart';
import 'package:grocerycompare/data/store_details.dart';
import 'package:grocerycompare/service/map.dart';

import '../val.g.dart' as globals;

import 'package:flutter/material.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

class PlaceSourcePage extends StatelessWidget {
  const PlaceSourcePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const PlaceSymbolBody();
  }
}

class PlaceSymbolBody extends StatefulWidget {
  const PlaceSymbolBody({super.key});

  @override
  State<StatefulWidget> createState() => PlaceSymbolBodyState();
}

class PlaceSymbolBodyState extends State<PlaceSymbolBody> {
  PlaceSymbolBodyState();

  late MapboxMapController controller;

  Future<void> _onMapCreated(MapboxMapController controller) async {
    this.controller = controller;
    controller.onCircleTapped.add(onMarkTapped);

    var b = await controller.getVisibleRegion();
    var limit = 5;
  
    var o = globals.mapApiKey;
    if (o is! String){
      return;
    }

    var r = await MapService().getNearby(b, o, limit);

    drawMakers(r);
    drawSelf(globals.pos!);
    _showList(r);

    setState(() {
      globals.searchResults = r;
      globals.bounds = b;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onMarkTapped(e) {
    controller.moveCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        target: e.options.geometry!,
        zoom: globals.defaultZoom,
      ),
    ));

    setState(() {
      globals.tap = true;
      globals.selectedStoreDetails = _getDetailsFromData(e.data!["storeId"], e.data!["name"], e.data!["neigbbourhoodId"]);
    });
  }

  StoreDetails _getDetailsFromData(String id, String name, String neighborhood){
    return StoreDetails(storeId:id, storeName: name, storeNeightbourhood: neighborhood);
  }

  void drawMakers(SearchboxResponse r) {
    if (r.results == null) {
      return;
    }

    for (var item in r.results!) {
      controller.addCircle(
          CircleOptions(
              geometry: LatLng(
                item.geometry!.coordinates!.latitude!,
                item.geometry!.coordinates!.longitude!,
              ),
              circleColor: "#FF0000"),
          <String, String>{
            "name": item.properties!.name!,
            "storeId": item.properties!.mapbox_id!,
            "neigbbourhoodId": item.properties?.context?['neighborhood']['id']
          });
    }
  }

  void drawSelf(LatLng p) {
    controller.addCircle(
      CircleOptions(
          geometry: LatLng(
            p.latitude,
            p.longitude,
          ),
          circleColor: "#00FF00"),
    );
  }

  Future<void> _showList(SearchboxResponse r) async {
    if (!mounted) {
      return;
    }

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(16))),
      builder: (context) => DraggableScrollableSheet(
        initialChildSize: 0.4,
        minChildSize: 0.2,
        maxChildSize: 0.75,
        expand: false,
        builder: (_, controller) => Column(
          children: [
            Icon(
              Icons.remove,
              color: Colors.grey[600],
            ),
            Expanded(
              child: ListView.builder(
                controller: controller,
                itemCount: r.results?.length,
                itemBuilder: (_, index) {
                  return Card(
                    child: InkWell(
                      onTapDown: (details) => {
                        setState(() {
                          globals.tap = true;
                        })
                      },
                      onTap: () {
                        setState(() {
                          globals.selectedStoreDetails =
                          globals.selectedStoreDetails = _getDetailsFromData(r.results![index].properties!.mapbox_id!, r.results![index].properties!.name!, r.results![index].properties!.context!["neighborhood"]["id"]);
                              
                        });
                        Navigator.pop(context);
                      },
                      child: Text((r.results?[index].properties?.name == null)
                          ? ""
                          : r.results![index].properties!.name!),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      MapboxMap(
        accessToken: globals.mapApiKey,
        onMapCreated: _onMapCreated,
        onMapClick: (point, coordinates) => {
          setState(() {
            globals.tap = false;
          })
        },
        initialCameraPosition: CameraPosition(
          target: LatLng(globals.pos!.latitude, globals.pos!.longitude),
          zoom: 9,
        ),
      ),
      (globals.tap)
          ? MapInfo(storeName: (globals.selectedStoreDetails == null ) ? "" : globals.selectedStoreDetails!.storeName)
          : const SizedBox(height: 0, width: 0),
    ]);
  }
}
