import 'package:flutter/material.dart';
import 'package:grocerycompare/data/item.dart';
import '../val.g.dart' as globals;

class Pricelist extends StatefulWidget {
  const Pricelist({
    super.key,

  });


  @override
  State<Pricelist> createState() => _PricelistState();
}

class _PricelistState extends State<Pricelist> {
  @override
  Widget build(BuildContext context) {
    var groceryList = globals.groceriesAtStore;

    return (groceryList is! List<Item>) ? const CircularProgressIndicator(): ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: groceryList.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.local_grocery_store),
                title: Text("Item: ${groceryList[index].name}"),
                subtitle: Text("Store: ${groceryList[index].storeId }"),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(width: 15),
                  const Text("Tags: "),
                  const Text("Fruits, Snack") ,
                  const SizedBox(width: 8),
                  const Text("Weight: "),
                  Text("\$${groceryList[index].weight}"),
                  const SizedBox(width: 8),
                  const Text("Cost: "),
                  Text("\$${groceryList[index].cost}"),
                ],
              ),
            ],
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}
