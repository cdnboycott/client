import 'package:flutter/material.dart';

class MapInfo extends StatelessWidget {
  const MapInfo({super.key, required this.storeName});

  final String storeName;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double windowWidth = 300;
    double windowHieght = 125;
    double bottomPadding = 15;

    return Positioned(
      bottom: bottomPadding,
      left: (size.width) / 2 - windowWidth / 2,
      child: Center(
        child: Card(
            child: SizedBox(
          width: windowWidth,
          height: windowHieght,
          child: Center(
              child: Text(storeName,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black.withOpacity(1),
                    decoration: TextDecoration.none,
                  ))),
        )),
      ),
    );
  }
}
