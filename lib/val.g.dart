library grocerycompare.globals;
                    
import 'dart:ffi';

import 'package:grocerycompare/data/item.dart';
import 'package:grocerycompare/data/searchbox.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

import 'data/store_details.dart';

import 'package:flutter_dotenv/flutter_dotenv.dart';

String? activeBarcode;
List<String> possibleTags = ["fruit"];
List<String> selectedTags = List.empty();
LatLng? pos;
String? mapApiKey = dotenv.env['MAP_API_KEY'];
StoreDetails? selectedStoreDetails;
String baseURL = "http://192.168.1.17/v1/";
LatLngBounds bounds = LatLngBounds(northeast: const LatLng(0, 0), southwest:const LatLng(0,0));
SearchboxResponse? searchResults;
bool tap = false;
double defaultZoom = 13;
List<Item>? groceriesAtStore;