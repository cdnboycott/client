import 'package:flutter/material.dart';
import 'package:grocerycompare/component/tags.dart';

/// Flutter code sample for [SingleChildScrollView].

void main() => runApp(const BootPage());

class BootPage extends StatelessWidget {
  const BootPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: SingleChildScrollViewExample(),
    );
  }
}

class SingleChildScrollViewExample extends StatelessWidget {
  const SingleChildScrollViewExample({super.key});
  

  @override
  Widget build(BuildContext context) {

        const Key centerKey = ValueKey<String>('bottom-sliver-list');

    return CustomScrollView(
        center: centerKey,
        slivers: <Widget>[
          SliverList(
            key: centerKey,
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  color:Colors.white,
                  height: 100,
                  child: TagSelectorField(),
                );
              },
              childCount: 1,
            ),
          ),
        ],
      );
  }
}
