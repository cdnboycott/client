import 'package:flutter/material.dart';
import 'package:grocerycompare/component/mapbox.dart';
import 'package:grocerycompare/service/position.dart';
import '../service/items.dart';
import '../val.g.dart' as globals;

class TargetPage extends StatefulWidget {
  const TargetPage({super.key});

  @override
  _TargetPageState createState() => _TargetPageState();
}

class _TargetPageState extends State<TargetPage>
    with SingleTickerProviderStateMixin {
  late AnimationController _loadingController;

  @override
  void initState() {
    super.initState();

    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {
        setState(() {});
      });
    _loadingController.repeat(reverse: true);

    getPosition();
    getItemsAtStore("store1");
  }

  @override
  void dispose() {
    _loadingController.dispose();
    super.dispose();
  }

  Future<void> getPosition() async {
    var p = await PositionService.determinePosition();
    if (!mounted) {
      return;
    }
    setState(() {
      globals.pos = p;
    });
  }

  Future<void> getItemsAtStore(String selectedStore) async {
    var p = await Items().getItemsAtStore(selectedStore);
    if (!mounted) {
      return;
    }
    setState(() {
      // TODO: Fix is fucking awful
      globals.groceriesAtStore = p;
    });
  }

  @override
  Widget build(BuildContext thcontext) {
    return Scaffold(
        body: (globals.pos == null)
            ? Center(
                child: CircularProgressIndicator(
                value: _loadingController.value,
                semanticsLabel: 'Circular progress indicator',
              ))
            : const PlaceSourcePage());
  }
}
