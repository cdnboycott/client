import 'package:flutter/material.dart';
import 'package:grocerycompare/component/mapbox.dart';
import 'package:grocerycompare/service/items.dart';
import 'package:grocerycompare/view/scanner.dart';
import '../component/tags.dart';
import '../val.g.dart' as globals;

const edgeInset = 5.0;

class AddPage extends StatefulWidget {
  const AddPage({super.key, required this.title});
  final String title;

  @override
  State<AddPage> createState() => _AddPageState();
}

const List<String> list = <String>['g', 'kg', 'lbs', 'l', 'gal', 'ml', 'fl oz'];

class _AddPageState extends State<AddPage> with SingleTickerProviderStateMixin {
  final nameController = TextEditingController();
  final costController = TextEditingController();
  final weightController = TextEditingController();
  final barcodeController = TextEditingController();
  final storeIdController = TextEditingController();
  final storeNameController = TextEditingController();
  final storeNeighborhoodController = TextEditingController();
  late AnimationController _loadingController;

  bool _saving = false;
  String? _snackMsg;
  String _dropdownValue = list.first;

  @override
  void initState() {
    super.initState();

    _loadingController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..addListener(() {
        setState(() {});
      });
    _loadingController.repeat(reverse: true);
  }

  @override
  void dispose() {
    _loadingController.dispose();
    super.dispose();
  }

  Widget _inputLabel(String title, TextEditingController ctrl) {
    return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10, top: 15),
        child: Material(
          child: TextFormField(
              decoration: InputDecoration(
                  border: const OutlineInputBorder(), label: Text(title)),
              controller: ctrl,
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return title;
                }
                return null;
              }),
        ));
  }

  Future<void> onSubmit() async {
    var data = <String, dynamic>{
      "barcode": barcodeController.value.text,
      "cost": double.parse(costController.value.text),
      "name": nameController.value.text,
      "unit": _dropdownValue,
      "weight": int.parse(weightController.value.text),
      "tags": List.empty(),
      "storeId": storeIdController.value.text,
      "storeName": storeNameController.value.text,
      "storeNeighborhood": globals.selectedStoreDetails!.storeNeightbourhood,
    };

    setState(() {
      _saving = true;
    });

    var success = await Items().postItemsAtStore(data);

    FocusManager.instance.primaryFocus?.unfocus();

    var snackMsg = "Item failed to save";

    if (success == true) {
      snackMsg = "Item Saved";
    }

    setState(() {
      _saving = false;
      _snackMsg = snackMsg;
    });

    if (_snackMsg != null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          action: SnackBarAction(
            label: 'Action',
            onPressed: () {
              // Code to execute.
            },
          ),
          content: Text(_snackMsg!),
          duration: const Duration(milliseconds: 1500),

          width: 280.0, // Width of the SnackBar.
          padding: const EdgeInsets.symmetric(
            horizontal: 8.0, // Inner padding for SnackBar content.
          ),
          behavior: SnackBarBehavior.floating,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    barcodeController.text = (globals.activeBarcode?.isEmpty ?? true)
        ? "-"
        : globals.activeBarcode.toString();
    storeIdController.text = (globals.selectedStoreDetails == null)
        ? ""
        : globals.selectedStoreDetails!.storeId;
    storeNameController.text = (globals.selectedStoreDetails == null)
        ? ""
        : globals.selectedStoreDetails!.storeName;
    storeNeighborhoodController.text = (globals.selectedStoreDetails == null)
        ? ""
        : globals.selectedStoreDetails!.storeNeightbourhood;

    return DefaultTextStyle(
        style: Theme.of(context).textTheme.bodyMedium!,
        child: LayoutBuilder(builder:
            (BuildContext context, BoxConstraints viewportConstraints) {
          return SingleChildScrollView(
              child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _inputLabel("Name", nameController),
              _inputLabel("Cost", costController),
              Row(children: [
                SizedBox(
                  width: 250.0,
                  child: _inputLabel("Weight", weightController),
                ),
                DropdownButton<String>(
                  value: _dropdownValue,
                  icon: const Icon(Icons.arrow_downward),
                  elevation: 16,
                  style: const TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String? value) {
                    // This is called when the user selects an item.
                    setState(() {
                      _dropdownValue = value!;
                    });
                  },
                  items: list.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                )
              ]),
              const TagSelectorField(),
              Row(children: [
                SizedBox(
                  width: 250.0,
                  child: _inputLabel("Barcode", barcodeController),
                ),
                ElevatedButton(
                  onPressed: () async {
                    Navigator.of(context)
                        .push(
                      MaterialPageRoute(
                        builder: (context) =>
                            const BarcodeScannerWithController(),
                      ),
                    )
                        .then((value) {
                      setState(() {});
                    });
                  },
                  child: const Text('Scan'),
                ),
              ]),
              Row(children: [
                SizedBox(
                  width: 250.0,
                  child: _inputLabel("Store Name", storeNameController),
                ),
                ElevatedButton(
                  onPressed: () async {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const PlaceSourcePage(),
                      ),
                    );
                  },
                  child: const Text('Map'),
                )
              ]),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16),
                    child: ElevatedButton(
                      onPressed: onSubmit,
                      child: const Text('Submit'),
                    ),
                  ),
                  (_saving)
                      ? CircularProgressIndicator(
                          value: _loadingController.value,
                          semanticsLabel: 'Circular progress indicator',
                        )
                      : const SizedBox.shrink(),
                ],
              )
            ],
          ));
        }));
  }
}
