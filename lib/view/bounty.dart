import 'package:flutter/material.dart';

class BountyPage extends StatefulWidget {
  const BountyPage({super.key, required this.title});
  final String title;

  @override
  State<BountyPage> createState() => _BountyPageState();
}

class _BountyPageState extends State<BountyPage> {
  final int _selectedIndex = 0;
  NavigationRailLabelType labelType = NavigationRailLabelType.all;
  bool showLeading = false;
  bool showTrailing = false;
  double groupAlignment = -1.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(children: [
      SizedBox(
          child: NavigationRail(
        selectedIndex: _selectedIndex,
        destinations: const <NavigationRailDestination>[
          NavigationRailDestination(
            icon: Icon(Icons.search_outlined),
            selectedIcon: Icon(Icons.search),
            label: Text('First'),
          ),
          NavigationRailDestination(
            icon: Icon(Icons.list_alt_outlined),
            selectedIcon: Icon(Icons.list_alt),
            label: Text('Second'),
          )
        ],
      )),
      const VerticalDivider(thickness: 1, width: 1),
      Expanded(
          child: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.barcode_reader),
                  title: Text('Search by barcode'),
                  subtitle: Text('Search by barcode'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    TextButton(
                      child: const Text('Scan'),
                      onPressed: () {/* ... */},
                    ),
                    const SizedBox(width: 8),
                  ],
                ),
              ],
            ),
          ),
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.album),
                  title: Text('Search by Name'),
                  subtitle: Text('Search by name'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                        child: TextField(
                      onChanged: (value) => {print(value)},
                    ))
                  ],
                ),
              ],
            ),
          )
        ],
      )),
    ]));
  }
}
